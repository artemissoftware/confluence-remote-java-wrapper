package biz.artemis.confluence.xmlrpcwrapper;

import java.util.Hashtable;

public class UserForXmlRpc {
    String name = "name";
    String fullname = "fullname";
    String email = "email";
    String url = "url";

    /**
     * creates a User. Since the values already exist in the
     * hashtable we just wrap that and reference it rather than
     * copying all the values. This reduces possible typos.
     * @param userHT
     * @return a user obj
     */
    public static UserForXmlRpc create(Hashtable userHT) {
        UserForXmlRpc user = new UserForXmlRpc();
        user.setUserParams(userHT);
        return user;
    }

    /**
     * This is the Hashtable holding the instance variables associated with
     * a user. Confluence-XMLRPC expects a Hashtable.
     */
    Hashtable userParams = new Hashtable();

    public Hashtable getUserParams() {
        return userParams;
    }

    public void setUserParams(Object userParams) {
        this.userParams = (Hashtable) userParams;
    }

    public String getEmail() {
        return String.valueOf(userParams.get(email));
    }


    public void setName(String nameVal) {
        userParams.put(name, nameVal);
    }

    public void setFullname(String fullnameVal) {
        userParams.put(fullname, fullnameVal);
    }

    public void setEmail(String emailVal) {
        userParams.put(email, emailVal);
    }

    public void setUrl(String urlVal) {
        userParams.put(url, urlVal);
    }
}
