package biz.artemis.confluence.xmlrpcwrapper;

import java.util.Date;
import java.util.Hashtable;

/**
 * Encapsulates data and functionality for the UserInformation object described here:
 * <a href="https://developer.atlassian.com/display/CONFDEV/Remote+Confluence+Data+Objects#RemoteConfluenceDataObjects-userinformationUserInformation">
 * Remote Confluence Data Objects - UserInformation</a> 
 * <p/>
 * I'm going to implement the needed getters/setters for now, and provide access to the hashtable for everything else
 */
public class UserInformation {
	private static final String KEY_CREATIONDATE = "creationDate";
	private static final String KEY_USERNAME = "username";
	private Hashtable<String,Object> hashtable = new Hashtable<String, Object>(); //all data
	
    public static UserInformation create(Hashtable hashtable) {
        UserInformation info = new UserInformation();
        info.setHashtable(hashtable);
        return info;
    }
	
	// Getters and Setters 
	public String getUsername() {
		return (String) this.hashtable.get(KEY_USERNAME);
	}
	public void setUsername(String username) {
		this.hashtable.put(KEY_USERNAME, username);
	}
	public Date getCreationDate() {
		return (Date) this.hashtable.get(KEY_CREATIONDATE);
	}
	
	public String getCreationDateString() {
		return String.valueOf(this.hashtable.get(KEY_CREATIONDATE));
	}
	public void setCreationDate(Date creationDate) {
		this.hashtable.put(KEY_CREATIONDATE, creationDate);
	}
	public Hashtable getHashtable() {
		return hashtable;
	}
	public void setHashtable(Hashtable hashtable) {
		this.hashtable = hashtable;
	}
}
