package biz.artemis.confluence.xmlrpcwrapper.test;

import java.util.Hashtable;
import java.util.Vector;

import junit.framework.TestCase;

import org.apache.log4j.Logger;

import biz.artemis.confluence.xmlrpcwrapper.ConfluenceServerSettings;
import biz.artemis.confluence.xmlrpcwrapper.RemoteWikiBroker;

public class SearchTest extends TestCase {

    RemoteWikiBroker remoteWikiBroker;
    ConfluenceServerSettings confSettings;
    Logger log = Logger.getLogger(this.getClass());

    protected void setUp() throws Exception {
        super.setUp();    
        confSettings = new ConfluenceServerSettings();
        confSettings.login="mylogin";
        confSettings.password="mypass";
        confSettings.url="localhost:8082";
        remoteWikiBroker = RemoteWikiBroker.getInstance();
    }
	
	public void testSearch() {
		String query = "friday";
		String expectedFirstTitle = "June15_2005";
		int expectedNumberOfResults = 4;
		
		Vector searchResultsTable;
		try {
			searchResultsTable = remoteWikiBroker.search(confSettings, query);
			assertNotNull("searchResults is NULL", searchResultsTable);
			
			assertTrue("Number of Results: " + searchResultsTable.size(),
					expectedNumberOfResults == searchResultsTable.size());
			
			assertTrue("Not a Hashtable, instead: " + searchResultsTable.get(0).getClass(),
					searchResultsTable.get(0) instanceof Hashtable);
			
			Hashtable firstResult = (Hashtable) searchResultsTable.get(0);
			String actualFirstTitle = (String) firstResult.get("title");
			
			assertEquals(expectedFirstTitle, actualFirstTitle);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		
	}

}
