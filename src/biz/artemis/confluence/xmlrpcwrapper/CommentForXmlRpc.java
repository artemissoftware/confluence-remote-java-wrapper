package biz.artemis.confluence.xmlrpcwrapper;

import java.util.Hashtable;

/**
 * Using this class which just wraps the Hashtable that Confluence returns for
 * a comment has several advantages including reducing errors from possible
 * String typos...also lets you use code complete in your favorite IDEA
 * and a Hashtable is a good thing to abstract.
 * NOTE: Based heavily on PageForXmlRpc
 */
public class CommentForXmlRpc {

    /**
     * key to get the id of the comment
     */
    public static final String ID = "id";
    /**
     * key to get the id of the page the comment belongs to
     */
    public static final String PAGE_ID = "pageId";
    /**
     * key to get the title of the comment
     */
    public static final String TITLE = "title";
    /**
     * key to get the url to view this comment online
     */
    public static final String URL = "url";
    /**
     * key to get the comment content
     */
    public static final String CONTENT = "content";
    /**
     * key to get the timestamp page was created
     */
    public static final String CREATED = "created";
    /**
     * key to get the username of the creator
     */
    public static final String CREATOR = "creator";

    /**
     * creates a CommentForXmlRpc. Since the values already exist in the
     * hashtable we just wrap that and reference it rather than
     * copying all the values. This reduces possible typos.
     * @param commentHT
     * @return a commentforxmlrpc obj
     */
    public static CommentForXmlRpc create(Hashtable commentHT) {
        CommentForXmlRpc comment = new CommentForXmlRpc();
        comment.setCommentParams(commentHT);
        return comment;
    }
    /**
     * This is the Hashtable holding the instance variables associated with
     * a comment. Confluence-XMLRPC expects a Hashtable.
     */
    Hashtable<String, String> commentParams = new Hashtable<String, String>();

    /**
     * @return table of comment params
     */
    public Hashtable<String, String> getCommentParams() {
        return commentParams;
    }

    /**
     * set all comment params, using the given table
     * @param commentParams 
     */
    public void setCommentParams(Hashtable<String, String> commentParams) {
        this.commentParams = commentParams;
    }

    /**
     * @return the id for this comment
     */
    public String getId() {
        return getParamVal(ID);
    }

    /**
     * @param idVal
     * @throws UnsupportedOperationException - we are providing this stub so:
     * (a) the API can be unchanged for the forseeable future, and (b)
     * to document that Confluence should be allowed to set the id for the ops
     * that the CRJW currently supports (addComment)
     */
    public void setId(String idVal) {
    	throw new UnsupportedOperationException("" +
    			"Confluence will set the id of the comment when we get or add it.");
    }

    /**
     * @return this comment's title
     */
    public String getTitle() {
        return getParamVal(TITLE);
    }

    /**
     * @param titleVal
     * @throws UnsupportedOperationException - we are providing this stub so:
     * (a) the API can be unchanged for the forseeable future, and (b)
     * to document that Confluence will set the title based on the 
     * associated pagename regardless of our efforts to change it
     */
    public void setTitle(String titleVal) {
        throw new UnsupportedOperationException("" +
        		"Confluence will set the title of the comment when we get or add it. " +
        		"Attempts to change it will be ignored by Confluence.");
    }

    /**
     * @return this comment's url
     */
    public String getUrl() {
        return getParamVal(URL);
    }

    /**
     * @param urlVal
     * @throws UnsupportedOperationException - we are providing this stub so:
     * (a) the API can be unchanged for the forseeable future, and (b)
     * to document that Confluence should be allowed to set the url for the ops
     * that the CRJW currently supports (addComment)
     */
    public void setUrl(String urlVal) {
    	throw new UnsupportedOperationException("" +
    			"Confluence will set the url of the comment when we get or add it.");
    }

    /**
     * @return this comment's content
     */
    public String getContent() {
        return getParamVal(CONTENT);
    }

    /**
     * set the content of this comment
     * @param contentVal
     */
    public void setContent(String contentVal) {
        commentParams.put(CONTENT, contentVal);
    }

    /**
     * @return the date this content was created
     */
    public String getCreated() {
        return getParamVal(CREATED);
    }

    /**
     * @param createdVal
     * @throws UnsupportedOperationException - we are providing this stub so:
     * (a) the API can be unchanged for the forseeable future, and (b)
     * to document that Confluence will set the date when the comment
     * is created regardless of our efforts to change it
     */
    public void setCreated(String createdVal) {
    	throw new UnsupportedOperationException("" +
    			"Confluence will set the created date of the comment when we add it. " +
    			"Attempts to change it will be ignored by Confluence.");
    }

    /**
     * @return this comment's creator's username
     */
    public String getCreator() {
        return String.valueOf(commentParams.get(CREATOR));
    }

    /**
     * @param creatorVal
     * @throws UnsupportedOperationException - we are providing this stub so:
     * (a) the API can be unchanged for the forseeable future, and (b)
     * to document that Confluence will set the creator based on the 
     * logged in user when the comment is created regardless of our efforts to change it
     */
    public void setCreator(String creatorVal) {
    	throw new UnsupportedOperationException("" +
    			"Confluence will set the creator of the comment when we add it. " +
    			"Attempts to change it will be ignored by Confluence.");
    }

    public String toString() {
        return this.getTitle();
    }
    
    public String getPageId() {
		return getParamVal(PAGE_ID);
    }

    
    /**
     * sets the page id this comment is associated with
     * @param pageId
     */
    public void setPageId(String pageId) {
    	commentParams.put(PAGE_ID, pageId);
    }

    /**
	 * @param key
	 * @return a comment field value for the given key or null
	 * if that key is not currently associated with a value
	 */
	public String getParamVal(String key) {
		if (commentParams.get(key) == null) return null;
    	return String.valueOf(commentParams.get(key));
	}

}
